/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: mdc_servo_decoder_fsm.sv
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Control logic for the servo feedback decoder

	This module consists of two finite state automata. One is responsible for
	sending individual bytes through the UART interface and the other one
	controls what data to be sent. The UART FSM is nested inside the main FSM.
*/

// TODO: Maybe move UART initialisation from the main FSM to the UART FSM
// TODO: Handle error and ready signals from the quadrature decoders

`timescale 1 ps / 1 ps

typedef enum logic [3:0] {
	RESET,
	EN_UART_IRQ,
	CLR_UART_STATUS,
	SEND_INIT_MSG,
	WAIT_0, WAIT_1,
	HOME_0, HOME_1,
	UPD_START_0, UPD_START_1,
	UPD_0, UPD_1
} servo_fb_state_t;

typedef enum logic[2:0] {
	IDLE,
	WRITE,
	WAIT_TRDY
} uart_state_t;

module mdc_servo_decoder_fsm (
		output logic [7:0]  avm_address,     //     avm.address
		output wire         avm_read,        //        .read
		input  wire  [31:0] avm_readdata,    //        .readdata
		output logic        avm_write,       //        .write
		output logic [31:0] avm_writedata,   //        .writedata
		input  wire         avm_waitrequest, //        .waitrequest
		output wire  [3:0]  avm_byteenable,  //        .byteenable
		input  wire         ready,           // rot_enc.ready
		input  wire         error,           //        .error
		input  wire  [31:0] m0_pos,          //        .m0_pos
		input  wire  [31:0] m1_pos,          //        .m1_pos
		input  wire         rst,             //     rst.reset_n
		input  wire         clk,             //     clk.clk
		input  wire         irq_uart         //     IRQ.irq
);
	// Chukcha ne chitatel, chukcha pisatel!
	assign avm_read = 1'b0;
	assign avm_byteenable = 4'b0011;

	wire m0_home;
	wire m1_home;
	// Detect home position
	assign m0_home = m0_pos[13:0] == 14'b00000000000000;
	assign m1_home = m1_pos[13:0] == 14'b00000000000000;
	
	wire [7:0]motor_positions[1:0][3:0];
	assign motor_positions[0][0] = m0_pos[7:0];
	assign motor_positions[0][1] = m0_pos[15:8];
	assign motor_positions[0][2] = m0_pos[23:16];
	assign motor_positions[0][3] = m0_pos[31:24];
	assign motor_positions[1][0] = m1_pos[7:0];
	assign motor_positions[1][1] = m1_pos[15:8];
	assign motor_positions[1][2] = m1_pos[23:16];
	assign motor_positions[1][3] = m1_pos[31:24];

	servo_fb_state_t servo_fb_state, servo_fb_next_state, servo_fb_tmp;
	uart_state_t uart_state, uart_next_state;
	logic [1:0]home_latch;
	logic [1:0]home_pos_reported;
	logic [31:0]clk_div;
	logic slow_clk;
	logic [2:0]bytes_sent;
	logic uart_write, uart_busy, prev_uart_busy;
	logic [7:0]byte_to_send;

	always_ff @(posedge clk or negedge rst) begin
		if (!rst) begin
			clk_div <= 32'h00000000;
			slow_clk <= 1'b0;
			uart_state <= IDLE;
			servo_fb_state <= RESET;
		end else begin
			// Divide the clock by 500000. With a 100 MHz clock this results
			// in 100Sps reporting of each motor's current position.
			if (clk_div == 499999) begin
				clk_div <= 32'h00000000;
				slow_clk <= 1'b1;
			end else begin
				slow_clk <= 1'b0;
				clk_div <= clk_div + 1'b1;
			end
			uart_state <= uart_next_state;
			servo_fb_state <= servo_fb_next_state;
		end
	end

	// Next state logic for UART FSM
	always_comb begin
		uart_next_state = uart_state;
		case (uart_state)
			IDLE: uart_next_state = uart_write ? WRITE : IDLE;
			WRITE: uart_next_state = !avm_waitrequest ? WAIT_TRDY : WRITE;
			WAIT_TRDY: uart_next_state = irq_uart ? IDLE : WAIT_TRDY;
		endcase
	end

	// Next state logic for main FSM
	always_comb begin
		case (servo_fb_state)
			RESET: servo_fb_next_state = EN_UART_IRQ;
			EN_UART_IRQ: servo_fb_next_state = avm_waitrequest ? EN_UART_IRQ : CLR_UART_STATUS;
			CLR_UART_STATUS: servo_fb_next_state = avm_waitrequest ? CLR_UART_STATUS : SEND_INIT_MSG;
			SEND_INIT_MSG: servo_fb_next_state = !uart_busy ? WAIT_0 : SEND_INIT_MSG;
			WAIT_0:
				if (home_latch[0])
					servo_fb_next_state = HOME_0;
				else if (home_latch[1])
					servo_fb_next_state = HOME_1;
				else if (slow_clk)
					servo_fb_next_state = UPD_START_0;
				else
					servo_fb_next_state = WAIT_0;
			UPD_START_0: servo_fb_next_state = !uart_busy ? UPD_0 : UPD_START_0;
			UPD_0:
				if (bytes_sent == 3'h4 && !uart_busy)
					servo_fb_next_state = WAIT_1;
				else
					servo_fb_next_state = UPD_0;
			WAIT_1:
				if (home_latch[0])
					servo_fb_next_state = HOME_0;
				else if (home_latch[1])
					servo_fb_next_state = HOME_1;
				else if (slow_clk)
					servo_fb_next_state = UPD_START_1;
				else
					servo_fb_next_state = WAIT_1;
			UPD_START_1: servo_fb_next_state = !uart_busy ? UPD_1 : UPD_START_1;
			UPD_1:
				if (bytes_sent == 3'h4 && !uart_busy)
					servo_fb_next_state = WAIT_0;
				else
					servo_fb_next_state = UPD_1;
			HOME_0: servo_fb_next_state = !uart_busy ? servo_fb_tmp : HOME_0;
			HOME_1: servo_fb_next_state = !uart_busy ? servo_fb_tmp : HOME_1;
		endcase
	end

	// Outputs of both FSMs
	always_ff @(posedge clk or negedge rst) begin
		if (!rst) begin
			home_latch <= 2'b00;
			home_pos_reported <= 2'b00;
			bytes_sent <= 3'b000;
			servo_fb_tmp <= WAIT_0;
			byte_to_send <= 8'h00;
			avm_address <= 8'h00;
			avm_writedata <= 32'h00000000;
			avm_write <= 1'b0;
			uart_busy <= 1'b0;
			prev_uart_busy <= 1'b0;
			uart_write <= 1'b0;
		end else begin
			// Latch the home position signals because the position may change while a regular position report
			// is taking place and we do not want to miss a home position report. Also, make sure home position
			// reports occur only once per motor per wait cycle.
			if (m0_home && !home_pos_reported[0]) begin
				home_pos_reported[0] <= 1'b1;
				home_latch[0] <= 1'b1;
			end
			if (m1_home && !home_pos_reported[1]) begin
				home_pos_reported[1] <= 1'b1;
				home_latch[1] <= 1'b1;
			end

			prev_uart_busy <= uart_busy;
			
			case (uart_next_state)
				IDLE: begin
					avm_address <= 8'h00;
					avm_writedata <= 32'h00000000;
					avm_write <= 1'b0;
					uart_busy <= 1'b0;
				end
				WRITE: begin
					avm_address <= 8'h24;
					avm_writedata[31:8] <= 24'h000000;
					avm_writedata[7:0] <= byte_to_send;
					avm_write <= 1'b1;
					uart_busy <= 1'b1;
					uart_write <= 1'b0;
				end
				WAIT_TRDY: begin
					avm_address <= 8'h00;
					avm_writedata <= 32'h00000000;
					avm_write <= 1'b0;
					uart_busy <= 1'b1;
				end
			endcase

			// Some of the following actions must only occur once per state. That is, they happen on the first clock cycle
			// of the corresponding state and do not on every other cycle that the FSM spends in that state.
			case (servo_fb_next_state)
				RESET:;
				EN_UART_IRQ: begin
					avm_address <= 8'h2C;
					avm_writedata[31:16] <= 16'h0000;
					avm_writedata[15:0] <= 16'h0040;
					avm_write <= 1'b1;
				end
				CLR_UART_STATUS: begin
					avm_address <= 8'h28;
					avm_writedata <= 32'h00000000;
					avm_write <= 1'b1;
				end
				SEND_INIT_MSG:
					if (servo_fb_state == CLR_UART_STATUS) begin
						avm_write <= 1'b0;
						byte_to_send <= 8'h08;
						uart_write <= 1'b1;
					end
				WAIT_0:
					servo_fb_tmp <= WAIT_0;
				UPD_START_0:
					if (servo_fb_state == WAIT_0) begin
						byte_to_send <= 8'h10;
						bytes_sent <= 3'b000;
						uart_write <= 1'b1;
						home_pos_reported <= 2'b00;
					end
				UPD_0:
					if (!uart_busy && prev_uart_busy) begin
						byte_to_send <= motor_positions[0][bytes_sent];
						bytes_sent <= bytes_sent + 1'b1;
						uart_write <= 1'b1;
					end
				WAIT_1:
					servo_fb_tmp <= WAIT_1;
				UPD_START_1:
					if (servo_fb_state == WAIT_1) begin
						byte_to_send <= 8'h20;
						bytes_sent <= 3'b000;
						uart_write <= 1'b1;
						home_pos_reported <= 2'b00;
					end
				UPD_1:
					if (!uart_busy && prev_uart_busy) begin
						byte_to_send <= motor_positions[1][bytes_sent];
						bytes_sent <= bytes_sent + 1'b1;
						uart_write <= 1'b1;
					end
				HOME_0:
					if (servo_fb_state == WAIT_0 || servo_fb_state == WAIT_1) begin
						byte_to_send <= 8'h40;
						uart_write <= 1'b1;
						home_latch[0] <= 1'b0;
					end
				HOME_1:
					if (servo_fb_state == WAIT_0 || servo_fb_state == WAIT_1) begin
						byte_to_send <= 8'h80;
						uart_write <= 1'b1;
						home_latch[1] <= 1'b0;
					end
			endcase
		end
	end
endmodule
