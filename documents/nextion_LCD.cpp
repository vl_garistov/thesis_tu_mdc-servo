/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: nextion_LCD.cpp
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Reading and writing data to and from the GUI on the Nextion LCD
*/

#include "nextion_LCD.h"
#include "Arduino.h"
#include "src/ITEADLIB_Arduino_Nextion/Nextion.h"
#include "dual_servo.h"
#include <EEPROM.h>
#include "eeprom_addresses.h"
#include <inttypes.h>
#include <math.h>

// TODO: 404, 405 and mostly 406 errors occur ocassionally. Figure out why.
// Error 403 occurs while keyboard is open on screen.

// pointer to the data that is output as read-only in the GUI (motor positions)
extern volatile const uint32_t * const motor_pos_ptr;
extern volatile const uint32_t * const motor_real_pos_ptr;

static volatile bool nextion_LCD_update_flag = false;

static nextion_GUI_t nextion_GUI =
{
	.pos =
	{
		NexNumber(0, 1, "x0"),
		NexNumber(0, 3, "x2")
	},
	.speed = NexNumber(0, 2, "x1"),
	.macros =
	{
		NexNumber(0, 4, "x3"),
		NexNumber(0, 5, "x4"),
		NexNumber(0, 6, "x5"),
		NexNumber(0, 7, "x6"),
		NexNumber(0, 8, "x7"),
		NexNumber(0, 9, "x8"),
		NexNumber(0, 10, "x9"),
		NexNumber(0, 11, "x10"),
		NexNumber(0, 12, "x11"),
		NexNumber(0, 13, "x12"),
		NexNumber(0, 14, "x13"),
		NexNumber(0, 15, "x14"),
		NexNumber(0, 16, "x15"),
		NexNumber(0, 17, "x16"),
		NexNumber(0, 18, "x17"),
		NexNumber(0, 19, "x18")
	}
};

// the list of posiible moves KUKA can request. Editable by the GUI. Speed is in rpm.
static volatile GUI_params_t GUI_params =
{
	.speed = DEFAULT_SPEED,
	.macros = {0}
};

extern volatile const GUI_params_t * const GUI_params_ptr = &GUI_params;

// Initialises the Nextion display. Must be called in setup()
int nextion_LCD_init(void)
{
	// Read previously saved values from the EEPROM
	EEPROM.get(ADDR_SPEED, (float&) GUI_params.speed);
	for (uint8_t i = 1; i < TOTAL_MACROS; i++)
	{
		// Adress is ofset by 1 because the first macro is reserved and not written to EEPROM
		EEPROM.get(ADDR_MACROS + sizeof(*GUI_params.macros) * (i - 1), (int32_t&) GUI_params.macros[i]);
	}

	if (!nexInit(NEXTION_BAUDRATE_MAIN, NEXTION_BAUDRATE_DEBUG))
		return 401;
	// Don't remove this! The Nextion display takes a while to initialise properly and throws an error if
	// it receives anything too soon after the call to nexInit().
	delay(1000);
	if (!nextion_GUI.speed.setValue(GUI_params.speed * NEXTION_SPEED_DISPLAY_MULTIPLIER))
		return 402;

	delay(5);
	if (!nextion_GUI.pos[0].Set_background_color_bco(motor_calibrated(0) ? NEXTION_BLUE : NEXTION_RED))
		return 408;
	delay(5);
	if (!nextion_GUI.pos[1].Set_background_color_bco(motor_calibrated(1) ? NEXTION_BLUE : NEXTION_RED))
		return 408;

	for (uint8_t i = 0; i < TOTAL_MACROS; i++)
	{
		delay(5);
		if (!nextion_GUI.macros[i].setValue(GUI_params.macros[i]))
			return 407;
	}

	cli();
	// Clear Timer/Counter Control Registers
	TCCR5A = 0;
	TCCR5B = 0;
	TIMSK5 = 0;
	OCR5B = 0;
	OCR5C = 0;
	// Set CTC Mode
	TCCR5B |= (1 << WGM52);
	// Clear Counter
	TCNT5  = 0;
	// Set frequency/top value - 8 Hz
	// Nextion polling should be slow. In theory at 115200 baud polling at 20 Hz should be perfectly fine.
	// In practice even at 5 Hz there are ocassional glitches. Probably the MCU on the Nextion display
	// can't process the data fast enough to allow full use of the serial bandwidth.
	// Note: baudrates, other than 115200, cause problems. Maybe it's the baudrate and not the processing resource?
	OCR5A = 31249;
	// Enable compare match interrupt
	TIMSK5 |= (1 << OCIE5A);
	// Set prescaler to 64 and start the timer
	TCCR5B |= (1 << CS51) | (1 << CS50);
	//TCCR5B |= (1 << CS52);
	sei();

	return 0;
}

// Periodically check if it's time to refresh the displayed values.
// Must be called relatively often in loop()
int nextion_LCD_update(void)
{
	int32_t buf = 0;
	float new_speed, current_speed;
	int32_t new_macro, current_macro;
	// Yes, we actually need this. An intermediate value in the following calculations can be around 5 billion.
	int64_t pos_buf;
	static GUI_params_t prev_GUI_params =
	{
		.speed = DEFAULT_SPEED,
		.macros = {0}
	};

	// Sometimes the Nextion display returns zeros instead of the actual data.
	// Because of this values in GUI_params are updated only after reading the same value twice in a row.
	if (nextion_LCD_update_flag)
	{
		nextion_LCD_update_flag = false;

		cli();
		pos_buf = motor_real_pos_ptr[0];
		sei();
		pos_buf = (pos_buf * 360 * NEXTION_POS_DISPLAY_MULTIPLIER) / PULSES_PER_ROTATION - GLOBAL_OFFSET * NEXTION_POS_DISPLAY_MULTIPLIER;
		if (!nextion_GUI.pos[0].setValue(pos_buf))
			return 403;
		delay(5);

		cli();
		pos_buf = motor_real_pos_ptr[1];
		sei();
		pos_buf = (pos_buf * 360 * NEXTION_POS_DISPLAY_MULTIPLIER) / PULSES_PER_ROTATION - GLOBAL_OFFSET * NEXTION_POS_DISPLAY_MULTIPLIER;
		if (!nextion_GUI.pos[1].setValue(pos_buf))
			return 404;
		delay(5);

		if (!nextion_GUI.pos[0].Set_background_color_bco(motor_calibrated(0) ? NEXTION_BLUE : NEXTION_RED))
			return 408;
		delay(5);
		if (!nextion_GUI.pos[1].Set_background_color_bco(motor_calibrated(1) ? NEXTION_BLUE : NEXTION_RED))
			return 408;
		delay(5);

		if (!nextion_GUI.speed.getValue(&buf))
			return 405;
		new_speed = (float) buf / NEXTION_SPEED_DISPLAY_MULTIPLIER;
		if (new_speed == prev_GUI_params.speed)
		{
			// Every time an operator changes the speed, save it to EEPROM
			current_speed = GUI_params.speed;
			if (new_speed != current_speed)
			{
				if (new_speed < 0.0 || new_speed > MAX_SPEED)
				{
					delay(5);
					if (!nextion_GUI.speed.setValue(current_speed * NEXTION_SPEED_DISPLAY_MULTIPLIER))
						return 410;
				}
				else
				{
					cli();
					GUI_params.speed = new_speed;
					sei();
					EEPROM.put(ADDR_SPEED, new_speed);
				}
			}
		}
		prev_GUI_params.speed = new_speed;

		for (uint8_t i = 1; i < TOTAL_MACROS; i++)
		{
			delay(5);

			if (!nextion_GUI.macros[i].getValue(&new_macro))
				return 406;
			if (new_macro == prev_GUI_params.macros[i])
			{
				// Every time an operator changes a macro, save it to EEPROM
				current_macro = GUI_params.macros[i];
				if (new_macro != current_macro)
				{
					#ifdef GUI_ABSOLUTE
					if (new_macro < -(GLOBAL_OFFSET * NEXTION_ANGLE_DISPLAY_MULTIPLIER) || new_macro > (360L - GLOBAL_OFFSET) * NEXTION_ANGLE_DISPLAY_MULTIPLIER - 4)
					#else
					// These limits prevent integer overflow in attempt_next_macro(). They are rounded down to a
					// nice even number of rotations
					if (new_macro < -360000L || new_macro > 360000L)
					#endif
					{
						delay(5);
						if (!nextion_GUI.macros[i].setValue(current_macro))
							return 409;
					}
					else
					{
						cli();
						GUI_params.macros[i] = new_macro;
						sei();
						// Adress is ofset by 1 because the first macro is reserved and not written to EEPROM
						EEPROM.put(ADDR_MACROS + (i - 1) * sizeof(*GUI_params.macros), new_macro);
					}
				}
			}
			prev_GUI_params.macros[i] = new_macro;
		}
	}

	return 0;
}

ISR(TIMER5_COMPA_vect)
{
	nextion_LCD_update_flag = true;
}
