/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: PLC.ino
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Main file of the PLC program
*/

#include "src/RS232_custom/RS232_custom.h"
#include "dual_servo.h"
#include "nextion_LCD.h"
#include "stacklight.h"
#include "buzzer.h"
#include "motor_macro.h"
#include "log_error.h"
#include <errno.h>

static void hardware_init(void);
static void test1(void);
static void test2(void);
static void test3(void);

extern volatile const uint32_t * const motor_real_pos_ptr;

// Executes once at startup
void setup(void)
{
	hardware_init();

	// When new code is uploaded onto the controller, for a brief time the old code executes.
	// This prevents motor movement from the test functions during this time
	//delay(3000);
	//test1();
	//test2();
	//test3();
}

// Executes in an infinite loop after setup()
void loop(void)
{
	int err;
	// The Nextion LCD must be polled because there were issues when interrupts were used
	err = nextion_LCD_update();
	if (err)
		log_error(err, "An error was encountered while updating the data on the Nextion LCD", false);
	if (errno)
	{
		if (errno == EBUSY)
			log_error(1, "Attempted start of motor in wrong mode. Check the mode selection switch", false);
		else if (errno == EACCES)
			log_error(1, "Attempted start of uncalibrated motor", false);
		else
			log_error(1, "Unexpected error", true);
		errno = 0;
	}
	dual_servo_loop();
}

// Initializatioon of connected hardware
static void hardware_init(void)
{
	int err;

	Serial.begin(115200);
	RS232_custom.begin(38400, SERIAL_8E1);

	err = dual_servo_init();
	if (err)
		log_error(err, "Failed to initialise the dual servo drivers", true);
	/*
	err = soft_start_disable();
	if (err)
		log_error(err, "Failed to initialise the dual servo drivers", true);
	*/
	err = nextion_LCD_init();
	if (err)
		log_error(err, "Failed to initialise the Nextion LCD", true);
	// The stacklight and buzzer were removed from the project because they require output pins on the PLC
	// that can sink current. The PLC that we are using can only source current from its outputs.
	/*
	err = buzzer_init();
	if (err)
		log_error(err, "Failed to initialise buzzer", true);
	stacklight_init();
	*/
}

static void test1(void)
{
	start_motor(0, ANTI_CLOCKWISE, ROTATE_FOREVER, 3.0);
}

static void test2(void)
{
	int err;
	uint32_t pos;

	for (uint16_t i = 0; i < 4; i++)
	{
		start_motor(0, CLOCKWISE, STEPS_PER_ROTATION / 4, 5.0);
		while (motor_active(0))
		{
			err = nextion_LCD_update();
			if (err)
				log_error(err, "An error was encountered while updating the data on the Nextion LCD", false);
		}
		delay(1000);
		cli();
		pos = motor_real_pos_ptr[0];
		sei();
		pos /= 4;
		Serial.println(pos);
		err = nextion_LCD_update();
		if (err)
			log_error(err, "An error was encountered while updating the data on the Nextion LCD", false);
		delay(1000);
	}
}

static void test3(void)
{
	start_motor(0, ANTI_CLOCKWISE, STEPS_PER_ROTATION, 3.0);
}