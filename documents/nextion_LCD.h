/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: nextion_LCD.h
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Reading and writing data to and from the GUI on the Nextion LCD
*/

#ifndef NEXTION_LCD_H
#define NEXTION_LCD_H

#include "dual_servo.h"
#include "motor_macro.h"
#include "src/ITEADLIB_Arduino_Nextion/Nextion.h"

#define NEXTION_BAUDRATE_MAIN	115200
#define NEXTION_BAUDRATE_DEBUG	9600
#define NEXTION_LCD_START_BYTE	0xBC
#define NEXTION_LCD_END_BYTE	0xFF
#define NEXTION_LCD_BUFF_SIZE	5
#define NEXTION_LCD_ID_OFFSET	4
#define NEXTION_LCD_SPEED_ID	2

#define NEXTION_SPEED_DISPLAY_MULTIPLIER	100.0
#define NEXTION_ANGLE_DISPLAY_MULTIPLIER	100L
#define NEXTION_POS_DISPLAY_MULTIPLIER		100L

#define NEXTION_BLUE	1048
#define NEXTION_RED		57352

// Handles to the GUI objects
typedef struct
{
	NexNumber pos[NUMBER_OF_MOTORS];
	NexNumber speed;
	NexNumber macros[TOTAL_MACROS];
}
nextion_GUI_t;

// Data that is input by the operator in the GUI and read by the PLC
typedef struct
{
	float speed;
	int32_t macros[TOTAL_MACROS];
}
GUI_params_t;

// Initialises the Nextion display. Must be called in setup()
int nextion_LCD_init(void);
// Periodically check if it's time to refresh the displayed values.
// Must be called relatively often in loop()
int nextion_LCD_update(void);

#endif