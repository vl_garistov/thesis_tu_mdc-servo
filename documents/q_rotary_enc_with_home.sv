/*
	Counter for quadrature rotary encoder.
	Module with low-pass filter for A, B and C signals is rot_enc_flt.sv
*/

`ifndef _q_rotary_enc_with_home_
`define _q_rotary_enc_with_home_

module q_rotary_enc_with_home #(parameter RESOLUTION) (
	input clock, sclr, ena,
	input dir, // main count direction. 0 - direct count, 1 - reverse
	input A, B, C, // signals from sensor. Recommended connection through optocouplers
	output reg signed [31:0] bidir_counter, // current relative position
	output reg error // A, B error. For to clear error use sclr signal
);

	localparam bit[1:0] grey_inc2[4] = '{2'b01, 2'b11, 2'b00, 2'b10};
	localparam bit[1:0] grey_dec2[4] = '{2'b10, 2'b00, 2'b11, 2'b01};

	wire [1:0] cur_code;
	reg [1:0] old_code = '0;
	
	assign cur_code = (dir) ? {A, B} : {B, A};
	
	always_ff @(posedge clock)
		old_code <= cur_code;
	
	wire cnt_ena = old_code != cur_code;
	wire inc = cnt_ena && cur_code == grey_inc2[old_code];
	wire dec = cnt_ena && cur_code == grey_dec2[old_code];
	wire err = cnt_ena && !(inc || dec);
	
	always_ff @(posedge clock) begin
		if (sclr)
			bidir_counter <= 'sh0;
		else if (ena)
			/*
				Can this be parametrised like the resolution?
				In this case C pulses 10 times per rotation because there is a
				10:1 gearbox and all code in the system works with the reduced speed.

				When a motor has reached its home position the 14 least significant bits are cleared and depending on the most significant
				one of them the value is rounded up or down. To round up, the number formed by the remaining bits is incremented but only
				if it won't overflow beyond the maximum position value determined by the emulated encoder resolution.
			*/
			if (C)
				if (bidir_counter[13])
					if (bidir_counter[17:14] == 4'h9)
						bidir_counter <= 32'sh00000000;
					else
						bidir_counter <= (bidir_counter & 32'hFFFFC000) + 32'h00004000;
				else
					bidir_counter <= bidir_counter & 32'hFFFFC000;
			// Incrementation/decrementation with overflow/underflow protection
			else if (inc)
				bidir_counter <= bidir_counter < RESOLUTION - 1'b1 ? bidir_counter + 1'b1 : 32'sh00000000;
			else if (dec)
				bidir_counter <= bidir_counter ? bidir_counter - 1'b1 : RESOLUTION - 1'b1;
	end

	always_ff @(posedge clock)
		if (sclr)
			error <= 1'b0;
		else if (ena && err)
			error <= 1'b1;
endmodule :q_rotary_enc_with_home

`endif
