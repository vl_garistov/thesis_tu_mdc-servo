/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: buzzer.cpp
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Functions for controlling the buzzer in the stacklight. UNUSED!
*/

#include "buzzer.h"
#include "Arduino.h"
#include <inttypes.h>

//#define DEBUG

static int set_buzzer_period(uint16_t period);
static int set_buzzer_pulse(float duty_cycle);

static volatile uint16_t remaining_pulses = 0;

// Initializes the buzzer, must be called in setup()
int buzzer_init(void)
{
	//Disable interrupts
	cli();
	
	pinMode(SL_BUZZER, OUTPUT);
	digitalWrite(SL_BUZZER, HIGH);
	
	remaining_pulses = 0;
	
	//Keep the prescaler under reset during configuration
	GTCCR |= (1 << TSM) | (1 << PSRSYNC);
	//Clear the counter
	TCNT1 = 0x00;
	//Set mode to non-inverted Phase and frequency correct PWM and set clock source to none
	TCCR1A = (1 << COM1B1) | (1 << WGM10);
	TCCR1B = 1 << WGM13;
	//Set period to 8.4s
	OCR1A = 0xFFFF;
	//Set pulse width to 4.2s
	OCR1B = 0x8888;
	//Enable timer interrupt
	TIMSK1 = 1 << TOIE1;
	//Clear the overflow interrupt
	TIFR1 |= 1 << TOV1;
	//Release timer reset
	GTCCR &= ~(1 << TSM);
	//Enable interrupts globally
	sei();

	// This dummy call ensures that the idle state of the buzzer pin is high.
	return buzz(0, 1, 0.0);
}

// Make n number of sound pulses with given period and duty cycle
int buzz(uint16_t n, uint16_t period, float duty_cycle)
{
	int err;

	// Check if buzzer is already running
	if (remaining_pulses)
		return 101;

	// buzzer_off clears the counter
	buzzer_off();
	// Configure frequency and duty cycle
	err = set_buzzer_period(period);
	if (err)
		return err;
	err = set_buzzer_pulse(duty_cycle);
	if (err)
		return err;
	// After turning on the buzzer the last interrupt from the previous buzzing is executed again.
	// To compensate, we count an extra pulse.
	n++;
	if (n)
		remaining_pulses = n;
	else
		return 102;
	buzzer_on();

	return 0;
}

// Configures the buzzer period.
// Note that this corresponds to the frequency of pulses from the buzzer, not the audio frequency.
static int set_buzzer_period(uint16_t period)
{
	if (!period || (period > MAX_BUZZER_PERIOD))
		return 103;

	OCR1A = (uint16_t) (period * TIM1_TICKS_PER_MS);

	return 0;
}

// Configures the buzzer duty cycle
static int set_buzzer_pulse(float duty_cycle)
{
	if (duty_cycle > 1.0 || duty_cycle < 0.0)
		return 104;

	OCR1B = (uint16_t) ((float) OCR1A * (1.0 - duty_cycle));

	return 0;
}

// executes at the end of every period
ISR(TIMER1_OVF_vect)
{
	#ifdef DEBUG
	static int first_run = 100;
	if (first_run)
	{
		first_run--;
		Serial.println(TIFR1 & (1 << TOV1));
	}
	#endif
	
	// TODO check with a flag if it's a recurred interrupt from a previous run
	// and remove the ugly +1 compensation
	remaining_pulses--;

	#ifdef DEBUG
	Serial.println(remaining_pulses);
	Serial.println(millis());
	#endif

	if (!remaining_pulses)
		buzzer_off();
}
