/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: dual_servo.cpp
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Functions for controlling the servo drivers
*/

#include "dual_servo.h"
#include "Arduino.h"
#include <EEPROM.h>
#include "eeprom_addresses.h"
#include "src/RS232_custom/RS232_custom.h"
#include "nextion_LCD.h"
#include "motor_macro.h"
#include "log_error.h"
#include <inttypes.h>
#include <stdlib.h>
#include <errno.h>

//#define DEBUG

static bool motor_ready(uint8_t motor);
static int check_mode(void);
static int modify_speed(uint8_t motor, float speed_change);
static int convert_speed_to_timer_period(float speed, uint16_t *period);
static int attempt_next_macro(void);
static void KUKA_ISR(void);
static void MOTOR0_PREHOME_POS_ISR(void);
static void MOTOR1_PREHOME_POS_ISR(void);
static void MOTOR0_HOME_POS_ISR(void);
static void MOTOR1_HOME_POS_ISR(void);
static void MOTOR_RDY_ISR(void);
static void MANUAL_FORWARD_ISR(void);
static void MANUAL_BACKWARD_ISR(void);
static void FAILSAFE_ISR(void);

extern volatile const GUI_params_t * const GUI_params_ptr;

// a queue for commands from KUKA
static volatile macro_queue_t *macro_queue = NULL;

static const dual_servo_t dual_servo_default =
{
	.remaining_steps = {0},
	.pos = {0},
	.real_pos = {0},
	.real_pos_offset = {0},
	.speed = 0.0,
	.dir = {FORWARD, FORWARD},
	.ready = {false, false},
	.calibrated = {false, false},
	.mode = kuka,				// the mode of operation of the motors - manual, KUKA-controlled or disabled
	.selected_motor = 0,
	.stop_at_home = false,
	.close_to_home = false,
	.speed_increment = 0.0,
	.soft_start = true,
	.soft_start_tmp_disabled = false,
	.soft_start_stage = 0,
	.steps_to_next_stage = 0,
	.finished_accelerating = false
};

static volatile dual_servo_t dual_servo = dual_servo_default;
static volatile button_states_t button_states = {.forward = 1, .backward = 1};
static volatile uint8_t PCI2_states = 0x00;
static volatile bool eeprom_save_rq = false;
static volatile bool eeprom_invalid_rq = false;

extern volatile const uint32_t * const motor_pos_ptr = dual_servo.pos;
extern volatile const uint32_t * const motor_real_pos_ptr = dual_servo.real_pos;

// Initializes the two servo drivers and the associated I/O. Must be called in setup()
int dual_servo_init(void)
{
	bool valid_saved_state = false;

	//Disable interrupts
	cli();

	pinMode(KUKA_MACRO0, INPUT);
	pinMode(KUKA_MACRO1, INPUT);
	pinMode(KUKA_MACRO2, INPUT);
	pinMode(KUKA_MACRO3, INPUT);
	pinMode(KUKA_MOTOR_SELECT, INPUT);
	pinMode(KUKA_EXECUTE, INPUT);
	pinMode(KUKA_MOTOR0_ACT, OUTPUT);
	digitalWrite(KUKA_MOTOR0_ACT, LOW);
	pinMode(KUKA_MOTOR1_ACT, OUTPUT);
	digitalWrite(KUKA_MOTOR1_ACT, LOW);

	pinMode(MOTOR0_STEP, OUTPUT);
	digitalWrite(MOTOR0_STEP, LOW);
	pinMode(MOTOR1_STEP, OUTPUT);
	digitalWrite(MOTOR1_STEP, LOW);
	pinMode(MOTOR0_DIR, OUTPUT);
	digitalWrite(MOTOR0_DIR, LOW);
	pinMode(MOTOR1_DIR, OUTPUT);
	digitalWrite(MOTOR1_DIR, LOW);
	pinMode(BREAK_RELEASE, OUTPUT);
	digitalWrite(BREAK_RELEASE, LOW);
	pinMode(MOTOR0_EN, OUTPUT);
	digitalWrite(MOTOR0_EN, LOW);
	pinMode(MOTOR1_EN, OUTPUT);
	digitalWrite(MOTOR1_EN, LOW);
	pinMode(MOTOR0_RDY, INPUT);
	pinMode(MOTOR1_RDY, INPUT);
	pinMode(MOTOR0_PREHOME_POS, INPUT);
	pinMode(MOTOR1_PREHOME_POS, INPUT);

	pinMode(MOTOR_SELECT, INPUT);
	pinMode(STOP_AT_HOME, INPUT);
	pinMode(MANUAL_FORWARD, INPUT);
	pinMode(MANUAL_BACKWARD, INPUT);
	pinMode(MANUAL_MODE, INPUT);
	pinMode(KUKA_MODE, INPUT);
	
	pinMode(REF_POINT_LED, OUTPUT);
	digitalWrite(REF_POINT_LED, HIGH);

	pinMode(EMSTOP, INPUT);

	// Individual external interrupts
	attachInterrupt(digitalPinToInterrupt(KUKA_EXECUTE), KUKA_ISR, CHANGE);
	attachInterrupt(digitalPinToInterrupt(MOTOR1_PREHOME_POS), MOTOR1_PREHOME_POS_ISR, RISING);
	attachInterrupt(digitalPinToInterrupt(MANUAL_FORWARD), MANUAL_FORWARD_ISR, CHANGE);
	attachInterrupt(digitalPinToInterrupt(MANUAL_BACKWARD), MANUAL_BACKWARD_ISR, CHANGE);
	// The rest of the external interrupts are grouped in PCI2
	PCMSK2 = (1 << PCINT16) | (1 << PCINT17) | (1 << PCINT18) | (1 << PCINT19);
	PCICR |= (1 << PCIE2);
	PCIFR |= (1 << PCIF2);

	PCI2_states = PINK;
	button_states.forward = digitalRead(MANUAL_FORWARD);
	button_states.backward = digitalRead(MANUAL_BACKWARD);
	memcpy((void *) &dual_servo, (void *) &dual_servo_default, sizeof(dual_servo_t));
	// This is necessary because MOTORx_RDY don't go low during PLC reset
	dual_servo.ready[0] = digitalRead(MOTOR0_RDY);
	dual_servo.ready[1] = digitalRead(MOTOR1_RDY);
	dual_servo.selected_motor = digitalRead(MOTOR_SELECT);
	dual_servo.stop_at_home = (bool) digitalRead(STOP_AT_HOME);

	errno = 0;

	//Keep the prescaler under reset during configuration
	GTCCR |= (1 << TSM) | (1 << PSRSYNC);
	//Clear the counter
	TCNT4 = 0x0;
	//Set mode to inverted Phase and frequency correct PWM and set clock source to system clock divided by 64 with prescaler
	TCCR4A = (1 << COM4B1) | (1 << COM4B0) | (1 << COM4C1) | (1 << COM4C0) | (1 << WGM40);
	TCCR4B = (1 << WGM43) | (1 << CS41) | (1 << CS40);
	//Set period to 16.384 ms
	OCR4A = 0x8888;
	//Set pulse width to 32.768 ms
	OCR4B = 0xFFFF;
	OCR4C = 0xFFFF;
	//Enable timer interrupt
	TIMSK4 = 1 << TOIE4;
	//Clear the overflow interrupt
	TIFR4 |= 1 << TOV4;
	//Release timer reset
	GTCCR &= ~(1 << TSM);

	macro_queue = mq_create(MACRO_QUEUE_LENGHT);
	if (macro_queue == NULL)
	{
		return 201;
	}

	// In order for this to work, the restored real position needs to be sent to the FPGA
	//EEPROM.get(ADDR_SAVED, (bool&) valid_saved_state);
	if (valid_saved_state)
	{
		EEPROM.get(ADDR_REAL_POS, (uint32_t&) dual_servo.real_pos[0]);
		EEPROM.get(ADDR_REAL_POS + sizeof(uint32_t), (uint32_t&) dual_servo.real_pos[1]);
		EEPROM.get(ADDR_OFFSET, (uint32_t&) dual_servo.real_pos_offset[0]);
		EEPROM.get(ADDR_OFFSET + sizeof(uint32_t), (uint32_t&) dual_servo.real_pos_offset[1]);
		EEPROM.put(ADDR_SAVED, false);
		dual_servo.calibrated[0] = dual_servo.calibrated[1] = true;
	}

	while (!digitalRead(MOTOR0_RDY));
	dual_servo.ready[0] = true;
	digitalWrite(MOTOR0_EN, HIGH);
	while (!digitalRead(MOTOR1_RDY));
	dual_servo.ready[1] = true;
	digitalWrite(MOTOR1_EN, HIGH);
	digitalWrite(BREAK_RELEASE, HIGH);

	//Enable interrupts globally
	sei();

	return 0;
}

// Rotate one of the motors (0 or 1) with a given number of steps
// speed is in rpm, direction is either CLOCKWISE or ANTI_CLOCKWISE (FORWARD or BACKWARD)
int start_motor(uint8_t motor, int8_t direction, int32_t steps, float speed)
{
	int err;
	uint16_t t;

	#ifdef DEBUG
	Serial.print("motor ");
	Serial.print(motor);
	Serial.print(", direction ");
	Serial.print(direction);
	Serial.print(", steps ");
	Serial.print(steps);
	Serial.print(", speed ");
	Serial.println(speed);
	#endif

	// Check mode and parameters
	err = check_mode();
	if (err)
		return err;

	if (dual_servo.mode == disabled)
		return 202;
	if (direction != FORWARD && direction != BACKWARD)
		return 203;
	
	if (steps < 1 && steps != ROTATE_FOREVER)
		return 204;

	if (dual_servo.soft_start)
	{
		// Soft start doesn't function correctly with too few steps
		if ((uint32_t) steps < SOFT_START_TOTAL_STEPS)
		{
			//return 205;
			dual_servo.soft_start_tmp_disabled = true;
			speed = DEFAULT_SPEED;
		}
		else
		{
			speed /= (float) SOFT_START_STAGES;
			dual_servo.soft_start_stage = 1;
			dual_servo.finished_accelerating = false;
			dual_servo.speed_increment = speed;
			dual_servo.steps_to_next_stage = SOFT_START_FIRST_STAGE_STEPS;
		}
	}

	err = convert_speed_to_timer_period(speed, &t);
	if (err)
		return err;

	if (motor == 0)
	{
		if (!motor_ready(0))
			return 210;
		if (dual_servo.remaining_steps[0])
			return 206;
		// Check if the other motor is running at a different speed
		// In order for both motors to run at the same time, it must be at the same speed!
		if (dual_servo.remaining_steps[1] && (t != OCR4A || dual_servo.soft_start))
			return 207;
		if (direction == FORWARD)
			digitalWrite(MOTOR0_DIR, HIGH);
		else if (direction == BACKWARD)
			digitalWrite(MOTOR0_DIR, LOW);
		else
			return 208;
		dual_servo.dir[0] = direction;
		// Set new timer period
		OCR4A = t;
		OCR4B =  t >> 1;
		dual_servo.remaining_steps[0] = steps;
		dual_servo.speed = speed;
		digitalWrite(KUKA_MOTOR0_ACT, HIGH);
	}
	else if (motor == 1)
	{
		if (!motor_ready(1))
			return 210;
		if (dual_servo.remaining_steps[1])
			return 206;
		// Check if the other motor is running at a different speed
		// In order for both motors to run at the same time, it must be at the same speed!
		if (dual_servo.remaining_steps[0] && (t != OCR4A || dual_servo.soft_start))
			return 207;
		if (direction == FORWARD)
			digitalWrite(MOTOR1_DIR, HIGH);
		else if (direction == BACKWARD)
			digitalWrite(MOTOR1_DIR, LOW);
		else
			return 208;
		dual_servo.dir[1] = direction;
		// Set new timer period
		OCR4A = t;
		OCR4C =  t >> 1;
		dual_servo.remaining_steps[1] = steps;
		dual_servo.speed = speed;
		digitalWrite(KUKA_MOTOR1_ACT, HIGH);
	}
	else
		return 209;
	return 0;
}

// Stop a motor that is currently moving
int stop_motor(uint8_t motor)
{
	if (motor == 0)
	{
		OCR4B = 0xFFFF;
		dual_servo.remaining_steps[0] = 0;
		digitalWrite(KUKA_MOTOR0_ACT, LOW);
		return 0;
	}
	else if (motor == 1)
	{
		OCR4C = 0xFFFF;
		dual_servo.remaining_steps[1] = 0;
		digitalWrite(KUKA_MOTOR1_ACT, LOW);
		return 0;
	}
	else
		return 210;
}

// Enable smooth acceleration and deceleration. It is enabled by default
int soft_start_enable(void)
{
	if (motor_active(0) || motor_active(1))
		return 211;

	dual_servo.soft_start = true;
	dual_servo.soft_start_tmp_disabled = false;
	dual_servo.soft_start_stage = 0;
	dual_servo.speed_increment = 0;
	dual_servo.finished_accelerating = false;
	dual_servo.steps_to_next_stage = 0;

	return 0;
}

// Disable smooth acceleration and deceleration
int soft_start_disable(void)
{
	if (dual_servo.remaining_steps[0] || dual_servo.remaining_steps[1])
		return 212;

	dual_servo.soft_start = false;
	dual_servo.soft_start_tmp_disabled = false;
	dual_servo.soft_start_stage = 0;
	dual_servo.speed_increment = 0;
	dual_servo.finished_accelerating = false;
	dual_servo.steps_to_next_stage = 0;

	return 0;
}

// Check if smooth acceleration and deceleration is enabled
bool soft_start_get(void)
{
	return dual_servo.soft_start;
}

// Check if a given motor is currently moving
bool motor_active(uint8_t motor)
{
	// TODO: handle error
	if (motor > 1)
		return false;
	return dual_servo.remaining_steps[motor] ? true : false;
}

// Check if a given motor is calibrated
bool motor_calibrated(uint8_t motor)
{
	// TODO: handle error
	if (motor > 1)
		return false;
	return dual_servo.calibrated[motor];
}

// Handle a byte of data from the FPGA feedback decoding module
void RS232_receive(uint8_t recv_byte)
{
	static RS232_buffer_t buf;
	static RS232_receiver_state_t state = IDLE;
	static uint8_t bytes_received = 0;

	buf.bytes[bytes_received] = recv_byte;
	switch (state)
	{
		case IDLE:
			switch (buf.bytes[bytes_received])
			{
				/*
					When a motor has reached its home position the 14 least significant bits are cleared and depending on the most significant
					one of them the value is rounded up or down. To round up, the number formed by the remaining bits is incremented but only
					if it won't overflow beyond the maximum position value determined by the emulated encoder resolution.
				*/
				case MOTOR_0_AT_HOME:
					if (dual_servo.real_pos[0] & 0x00002000)
						dual_servo.real_pos[0] = ((dual_servo.real_pos[0] & 0x0003C000) == 0x00024000) ? 0x00000000 : (dual_servo.real_pos[0] & 0xFFFFC000) + 0x00004000;
					else
						dual_servo.real_pos[0] = dual_servo.real_pos[0] & 0xFFFFC000;
					MOTOR0_HOME_POS_ISR();
					break;
				case MOTOR_1_AT_HOME:
					if (dual_servo.real_pos[1] & 0x00002000)
						dual_servo.real_pos[1] = ((dual_servo.real_pos[1] & 0x0003C000) == 0x00024000) ? 0x00000000 : (dual_servo.real_pos[1] & 0xFFFFC000) + 0x00004000;
					else
						dual_servo.real_pos[1] = dual_servo.real_pos[1] & 0xFFFFC000;
					MOTOR1_HOME_POS_ISR();
					break;
				case MOTOR_0_NEW_POS:
					state = UPD_MOTOR_0;
					break;
				case MOTOR_1_NEW_POS:
					state = UPD_MOTOR_1;
					break;
				case INIT_COMPLETE_MSG: break;
				default:;
			}
			break;
		case UPD_MOTOR_0:
			bytes_received++;
			if (bytes_received == 4)
			{
				if (buf.new_pos >= dual_servo.real_pos_offset[0])
					dual_servo.real_pos[0] = buf.new_pos - dual_servo.real_pos_offset[0];
				else
					dual_servo.real_pos[0] = buf.new_pos + PULSES_PER_ROTATION - dual_servo.real_pos_offset[0];
				bytes_received = 0;
				state = IDLE;
			}
			break;
		case UPD_MOTOR_1:
			bytes_received++;
			if (bytes_received == 4)
			{
				if (buf.new_pos >= dual_servo.real_pos_offset[1])
					dual_servo.real_pos[1] = buf.new_pos - dual_servo.real_pos_offset[1];
				else
					dual_servo.real_pos[1] = buf.new_pos + PULSES_PER_ROTATION - dual_servo.real_pos_offset[1];
				bytes_received = 0;
				state = IDLE;
			}
			break;
		default:;
	}
}

// Waits for the release of STOP_AT_HOME, if calibration has just finished
// Must be called in loop()
void dual_servo_loop(void)
{
	uint8_t motor;
	static unsigned long prev_blink = 0;
	unsigned long current_time;
	int err;

	if (dual_servo.mode == end_of_calibration)
	{
		motor = dual_servo.selected_motor;
		digitalWrite(REF_POINT_LED, LOW);
		delay(1000);
		dual_servo.pos[motor] = 0;
		dual_servo.close_to_home = false;
		dual_servo.real_pos_offset[motor] += dual_servo.real_pos[motor] + GLOBAL_OFFSET * PULSES_PER_ROTATION / 360L;
		dual_servo.real_pos_offset[motor] %= PULSES_PER_ROTATION;
		dual_servo.real_pos[motor] = 0;
		dual_servo.calibrated[motor] = true;
		// This does duplicate most of the loop() function but it's easier to implement than an entire new state.
		// Might want to put the errno handling in a separate function though.
		while (digitalRead(STOP_AT_HOME))
		{
			// The Nextion LCD must be polled because there were issues when interrupts were used
			err = nextion_LCD_update();
			if (err)
				log_error(err, "An error was encountered while updating the data on the Nextion LCD", false);
			if (errno)
			{
				if (errno == EBUSY)
					log_error(1, "Attempted start of motor in wrong mode. Check the mode selection switch", false);
				else if (errno == EACCES)
					log_error(1, "Attempted start of uncalibrated motor", false);
				else
					log_error(1, "Unexpected error", true);
				errno = 0;
			}
		}
		digitalWrite(REF_POINT_LED, HIGH);
		dual_servo.stop_at_home = false;
		// Disable the motors until check_mode() executes and refreshes the mode
		dual_servo.mode = disabled;
	}
	else if (dual_servo.stop_at_home)
	{
		current_time = millis();
		if (current_time - prev_blink > 500)
		{
			prev_blink = current_time;
			digitalWrite(REF_POINT_LED, !digitalRead(REF_POINT_LED));
		}
	}
	else
	{
		digitalWrite(REF_POINT_LED, HIGH);
	}
	// In order for this to work, the restored real position needs to be sent to the FPGA
	/*
	// Writing data to EEPROM is slow AF, must be done here instead of inside an ISR
	if (eeprom_save_rq)
	{
		uint32_t buff[2];

		eeprom_save_rq = false;
		// Unlike the GUI macros, position data is both read and written in ISRs so this is necessary
		cli();
		buff[0] = dual_servo.real_pos[0];
		buff[1] = dual_servo.real_pos[1];
		sei();

		EEPROM.put(ADDR_REAL_POS, buff[0]);
		EEPROM.put(ADDR_REAL_POS + sizeof(uint32_t), buff[1]);
		EEPROM.put(ADDR_OFFSET, PULSES_PER_ROTATION - buff[0]);
		EEPROM.put(ADDR_OFFSET + sizeof(uint32_t), PULSES_PER_ROTATION - buff[1]);
		EEPROM.put(ADDR_SAVED, true);
	}
	if (eeprom_invalid_rq)
	{
		eeprom_invalid_rq = false;
		EEPROM.put(ADDR_SAVED, false);
	}
	*/
}

// Check if a given motor's driver is asserting its RDY signal
static bool motor_ready(uint8_t motor)
{
	return dual_servo.ready[motor];
}

// Reads the mode select pins and updates the mode variable accordingly.
// Stops the motors and clears the queue when the mode has changed
static int check_mode(void)
{
	dual_servo_mode_t new_mode;
	uint8_t new_selected_motor = digitalRead(MOTOR_SELECT);
	bool stop_at_home_pressed;
	int err;
	uint16_t t;

	// Lock mode during the final stage of calibration
	if (dual_servo.mode == end_of_calibration)
		return 0;

	if (digitalRead(MANUAL_MODE))
		new_mode = manual;
	else if (digitalRead(KUKA_MODE))
		new_mode = kuka;
	else
		new_mode = disabled;
	if (new_mode != dual_servo.mode)
	{
		err = stop_motor(0);
		if (err)
			return err;
		err = stop_motor(1);
		if (err)
			return err;

		// Clear the queue
		err = mq_clear(macro_queue);
		if (err)
			return err;
		
		if (new_mode == disabled && dual_servo.calibrated[0] && dual_servo.calibrated[1])
			eeprom_save_rq = true;
		else if (dual_servo.mode == disabled)
			eeprom_invalid_rq = true;

		dual_servo.mode = new_mode;
		dual_servo.close_to_home = false;
	}
	else if (dual_servo.mode == manual && new_selected_motor != dual_servo.selected_motor)
	{
		if (new_selected_motor && motor_active(0))
		{
			err = convert_speed_to_timer_period(GUI_params_ptr->speed, &t);
			if (err)
				return err;
			OCR4C = t;
			dual_servo.remaining_steps[1] = ROTATE_FOREVER;
			digitalWrite(KUKA_MOTOR1_ACT, HIGH);
			stop_motor(0);
		}
		else if (motor_active(1))
		{
			err = convert_speed_to_timer_period(GUI_params_ptr->speed, &t);
			if (err)
				return err;
			OCR4B = t;
			dual_servo.remaining_steps[0] = ROTATE_FOREVER;
			digitalWrite(KUKA_MOTOR0_ACT, HIGH);
			stop_motor(1);
		}
		dual_servo.selected_motor = new_selected_motor;
		dual_servo.close_to_home = false;
	}
	stop_at_home_pressed = (bool) digitalRead(STOP_AT_HOME);
	dual_servo.stop_at_home = stop_at_home_pressed;

	return 0;
}

// Changes the speed of a motor. Used for implementing the smooth acceleration and deceleration
static int modify_speed(uint8_t motor, float speed_change)
{
	int conversion_error;
	float speed;
	uint16_t period;
	int err;

	if (!dual_servo.soft_start)
	{
		return 213;
	}
	if (motor > 1)
	{
		return 214;
	}

	speed = dual_servo.speed;
	speed += speed_change;
	dual_servo.speed = speed;
	conversion_error = convert_speed_to_timer_period(speed, &period);
	if (conversion_error)
	{
		err = stop_motor(motor);
		return err ? err : conversion_error;
	}
	else
	{
		#ifdef DEBUG
		Serial.println(speed);
		#endif

		OCR4A = period;
		if (motor == 0)
		{
			OCR4B =  period >> 1;
		}
		else
		{
			OCR4C =  period >> 1;
		}
	}

	return 0;
}

// Calculates the required timer period to obtain a given speed in rpm
static int convert_speed_to_timer_period(float speed, uint16_t *period)
{
	#ifdef DEBUG
	Serial.print("new speed: ");
	Serial.println(speed);
	#endif

	if (speed == USE_GUI_SPEED)
	{
		// Forgive me, Father Ritchie, for I have checked equality of floats.
		// It's fine though, comparing to 0 is reliable even for floats.
		speed = GUI_params_ptr->speed;
	}
	if (speed > MAX_SPEED || speed < MIN_SPEED)
		return 215;
	// Calculate timer peiod from speed
	*period = (uint16_t) ((125000.0 / ((speed / 60.0) * (float) STEPS_PER_ROTATION)) + 1.0);
	/*						|					 |
							|					 Seconds per minute
						Maximum PWM frequency with current timer configuration
	*/
	return 0;
}

// Attempts to execute the next macro in the queue. This may fail because the motor is busy.
// This is normal and expected.
static int attempt_next_macro(void)
{
	int32_t steps, angle, new_pos;
	const motor_macro_t * current_macro;
	int successful_start;	// 0 if successful
	int8_t dir;
	int err;

	// Decode direction from the sign
	current_macro = mq_get_first(macro_queue);
	if (current_macro == NULL)
		return 216;

	#ifdef GUI_ABSOLUTE
	angle = GUI_params_ptr->macros[current_macro->macro_num] + GLOBAL_OFFSET * NEXTION_POS_DISPLAY_MULTIPLIER;

	#ifdef DEBUG
	Serial.print("Starting macro ");
	Serial.print(current_macro->macro_num);
	Serial.print(" with angle ");
	Serial.print(angle);
	Serial.println(".");
	#endif

	new_pos = (angle * STEPS_PER_ROTATION) / (360L * NEXTION_ANGLE_DISPLAY_MULTIPLIER);
	
	if (new_pos > (int32_t) dual_servo.real_pos[current_macro->motor] >> 4)
	{
		steps = new_pos - (dual_servo.real_pos[current_macro->motor] >> 4);
		dir = FORWARD;
	}
	else
	{
		steps = (dual_servo.real_pos[current_macro->motor] >> 4) - new_pos;
		dir = BACKWARD;
	}
	#else // GUI_ABSOLUTE
	// Old code for relative movements
	// Macro 0 is reserved for returning to starting position
	if (current_macro->macro_num != 0)
	{
		angle = GUI_params_ptr->macros[current_macro->macro_num];
	
		#ifdef DEBUG
		Serial.print("Starting macro ");
		Serial.print(current_macro->macro_num);
		Serial.print(" with angle ");
		Serial.print(angle);
		Serial.println(".");
		#endif
	
		if (angle > 0)
		{
			steps = (angle * STEPS_PER_ROTATION) / (360L * NEXTION_ANGLE_DISPLAY_MULTIPLIER);
			dir = FORWARD;
		}
		else
		{
			steps = (-angle * STEPS_PER_ROTATION) / (360L * NEXTION_ANGLE_DISPLAY_MULTIPLIER);
			dir = BACKWARD;
		}
	}
	else
	{
		// The emulated encoder resolutiom (PULSES_PER_ROTATION) is 16 times larger than STEPS_PER_ROTATION
		steps = dual_servo.real_pos[current_macro->motor] >> 4;
		dir = BACKWARD;
	}
	#endif	// GUI_ABSOLUT

	// Don't execute the macro if the motor is already at the correct position
	if (steps == 0)
	{
		return mq_advance(macro_queue);
	}

	// Attempt to start the next macro and if successful, increment current macro counter
	cli();
	successful_start = start_motor(current_macro->motor, dir, steps, GUI_params_ptr->speed);
	if (!successful_start)
	{
		err = mq_advance(macro_queue);
		if (err)
		{
			sei();
			return err;
		}
	}
	else
	{
		#ifdef DEBUG
		Serial.print(successful_start);
		Serial.println(": Starting macro failed");
		#endif
	}
	sei();

	return successful_start;
}

// executed when KUKA requests a predefined move
static void KUKA_ISR(void)
{
	int err;
	// Software contact debounce
	static unsigned long last_activation_time = 0;
	unsigned long now = millis();

	// Overflows don't break this
	// Also, it's overkill. But boss wants it to be even longer.
	if (now - last_activation_time < 500)
		return;
	last_activation_time = now;

	uint8_t requested_macro = (digitalRead(KUKA_MACRO0) |
							  (digitalRead(KUKA_MACRO1) << 1) |
							  (digitalRead(KUKA_MACRO2) << 2) |
							  (digitalRead(KUKA_MACRO3) << 3));
	uint8_t selected_motor = digitalRead(KUKA_MOTOR_SELECT);

	err = check_mode();
	if (err)
		log_error(err, "Error in KUKA_ISR", true);
	if (dual_servo.mode != kuka)
	{
		errno = EBUSY;
		return;
	}
	if (!dual_servo.calibrated[selected_motor])
	{
		errno = EACCES;
		return;
	}

	// Add requested macro (predefined move) to the queue
	err = mq_add(macro_queue, selected_motor, requested_macro);
	if (err)
		log_error(err, "Error in KUKA_ISR", true);

	// It is normal for this call to fail, don't check for errors
	attempt_next_macro();
}

// Executes slightly before HOME_POS_ISR(). An additional inductive sensor triggers it.
// This function slows down the motor to prevent overshooting when seeking the home position.
static void MOTOR0_PREHOME_POS_ISR(void)
{
	uint16_t t;
	int err;

	if (dual_servo.stop_at_home && dual_servo.mode == manual &&
		!dual_servo.selected_motor && motor_active(0))
	{
		err = convert_speed_to_timer_period(SLOW_SPEED, &t);
		if (err)
			log_error(err, "Error in MOTOR0_PREHOME_POS_ISR", true);
		OCR4A = t;
		OCR4B = t >> 1;
		dual_servo.close_to_home = true;
	}
}

// Executes slightly before HOME_POS_ISR(). An additional inductive sensor triggers it.
// This function slows down the motor to prevent overshooting when seeking the home position.
static void MOTOR1_PREHOME_POS_ISR(void)
{
	uint16_t t;
	int err;
	
	if (dual_servo.stop_at_home && dual_servo.mode == manual &&
		dual_servo.selected_motor && motor_active(1))
	{
		err = convert_speed_to_timer_period(SLOW_SPEED, &t);
		if (err)
			log_error(err, "Error in MOTOR1_PREHOME_POS_ISR", true);
		OCR4A = t;
		OCR4C = t >> 1;
		dual_servo.close_to_home = true;
	}
}

// Executes when the resolver detects that the motor has reached its home position
static void MOTOR0_HOME_POS_ISR(void)
{
	uint16_t t = 0;
	int err;

	if (dual_servo.stop_at_home && dual_servo.mode == manual &&
		!dual_servo.selected_motor && motor_active(0) && dual_servo.close_to_home)
	{
		err = stop_motor(0);
		if (err)
			log_error(err, "Error in MOTOR0_HOME_POS_ISR", true);
		err = convert_speed_to_timer_period(GUI_params_ptr->speed, &t);
		if (err)
			log_error(err, "Error in MOTOR0_HOME_POS_ISR", true);
		OCR4A = t;
		dual_servo.mode = end_of_calibration;
	}
}

// Executes when the resolver detects that the motor has reached its home position
static void MOTOR1_HOME_POS_ISR(void)
{
	uint16_t t = 0;
	int err;

	if (dual_servo.stop_at_home && dual_servo.mode == manual &&
		dual_servo.selected_motor && motor_active(1) && dual_servo.close_to_home)
	{
		err = stop_motor(1);
		if (err)
			log_error(err, "Error in MOTOR1_HOME_POS_ISR", true);
		err = convert_speed_to_timer_period(GUI_params_ptr->speed, &t);
		if (err)
			log_error(err, "Error in MOTOR1_HOME_POS_ISR", true);
		OCR4A = t;
		dual_servo.mode = end_of_calibration;
	}
}

// Executes when the state of a driver's RDY signal changes
static void MOTOR_RDY_ISR(void)
{
	int err;

	if (dual_servo.ready[0] != digitalRead(MOTOR0_RDY))
	{
		if (dual_servo.ready[0])
		{
			dual_servo.ready[0] = false;
			err = stop_motor(0);
			if (err)
				log_error(err, "Error in MOTOR_RDY_ISR", true);
			digitalWrite(MOTOR0_EN, LOW);
		}
		else
		{
			dual_servo.ready[0] = true;
			digitalWrite(MOTOR0_EN, HIGH);
		}
	}
	if (dual_servo.ready[1] != digitalRead(MOTOR1_RDY))
	{
		if (dual_servo.ready[1])
		{
			dual_servo.ready[1] = false;
			err = stop_motor(1);
			if (err)
				log_error(err, "Error in MOTOR_RDY_ISR", true);
			digitalWrite(MOTOR1_EN, LOW);
		}
		else
		{
			dual_servo.ready[1] = true;
			digitalWrite(MOTOR1_EN, HIGH);
		}
	}
}

// Executes when the state of the button for forward movement changes
static void MANUAL_FORWARD_ISR(void)
{
	bool forward_btn = (bool) digitalRead(MANUAL_FORWARD);
	int err;

	err = check_mode();
	if (err)
		log_error(err, "Error in MANUAL_FORWARD_ISR", true);

	if (!forward_btn && button_states.forward)
	{
		// Forward button has been released
		err = stop_motor(dual_servo.selected_motor);
		if (err)
			log_error(err, "Error in MANUAL_FORWARD_ISR", true);
	}
	else if (forward_btn && !button_states.forward)
	{
		// Forward button has been pressed
		if (dual_servo.mode == manual)
		{
			err = start_motor(dual_servo.selected_motor, FORWARD, ROTATE_FOREVER, USE_GUI_SPEED);
			if (err)
				log_error(err, "Error in MANUAL_FORWARD_ISR", true);
		}
		else
		{
			errno = EBUSY;
		}
	}
	button_states.forward = forward_btn;
}

// Executes when the state of the button for backward movement changes
static void MANUAL_BACKWARD_ISR(void)
{
	bool backward_btn = (bool) digitalRead(MANUAL_BACKWARD);
	int err;

	err = check_mode();
	if (err)
		log_error(err, "Error in MANUAL_BACKWARD_ISR", true);

	if (!backward_btn && button_states.backward)
	{
		// Backward button has been released
		err = stop_motor(dual_servo.selected_motor);
		if (err)
			log_error(err, "Error in MANUAL_BACKWARD_ISR", true);
	}
	else if (backward_btn && !button_states.backward)
	{
		// Backward button has been pressed
		if (dual_servo.mode == manual)
		{
			err = start_motor(dual_servo.selected_motor, BACKWARD, ROTATE_FOREVER, USE_GUI_SPEED);
			if (err)
				log_error(err, "Error in MANUAL_BACKWARD_ISR", true);
		}
		else
		{
			errno = EBUSY;
		}
	}
	button_states.backward = backward_btn;
}

// Executes when the emergency stop button is pressed
static void FAILSAFE_ISR(void)
{
	int err1, err2;

	// Stop moving before you kill someone
	err1 = stop_motor(0);
	err2 = stop_motor(1);
	digitalWrite(MOTOR0_EN, LOW);
	digitalWrite(MOTOR1_EN, LOW);
	digitalWrite(BREAK_RELEASE, LOW);
	dual_servo.calibrated[0] = dual_servo.calibrated[1] = false;

	if (err1)
		log_error(err1, "Error in FAILSAFE_ISR", true);
	if (err2)
		log_error(err2, "Error in FAILSAFE_ISR", true);

	// Some old school delay for contact debounce
	for (volatile uint32_t i = 0; i < 10000000; i++);

	// Wait until the emergency has been dealt with
	while (!digitalRead(EMSTOP));

	// Re-enable the servo drivers
	while (!digitalRead(MOTOR0_RDY));
	dual_servo.ready[0] = true;
	digitalWrite(MOTOR0_EN, HIGH);
	while (!digitalRead(MOTOR1_RDY));
	dual_servo.ready[1] = true;
	digitalWrite(MOTOR1_EN, HIGH);
	digitalWrite(BREAK_RELEASE, HIGH);
}

// Execute at the end of every timer period (step)
ISR(TIMER4_OVF_vect)
{
	uint8_t i;
	int err;

	err = check_mode();
	if (err)
		log_error(err, "Error in TIMER4_OVF_vect", true);

	for (i = 0; i < 2; i++)
	{
		// Check if the motor should be rotating
		if (dual_servo.remaining_steps[i] > 0 || dual_servo.remaining_steps[i] == ROTATE_FOREVER)
		{
			// Update the position and remaining steps counters
			dual_servo.pos[i] = (dual_servo.pos[i] + dual_servo.dir[i] + STEPS_PER_ROTATION) % (uint32_t) STEPS_PER_ROTATION;
			if (dual_servo.remaining_steps[i] != ROTATE_FOREVER)
				dual_servo.remaining_steps[i]--;

			// Check if the motor should stop rotating
			if (!dual_servo.remaining_steps[i])
			{
				i == 1 ? OCR4C = 0xFFFF : OCR4B = 0xFFFF;
				dual_servo.soft_start_tmp_disabled = false;
				// Check if macro queue is empty and attempt to start next macro if it isn't
				if (dual_servo.mode == kuka && mq_get_size(macro_queue))
				{
					// It is normal for this call to fail, don't check for errors
					attempt_next_macro();
				}
				// Only indicate that the motor is no longer active if the next macro in queue didn't activate it again
				// Otherwise the MOTORx_ACT pin goes low for a very brief time that is hard to detect reliably in a noisy environment.
				if (!dual_servo.remaining_steps[i])
					i == 1 ? digitalWrite(KUKA_MOTOR1_ACT, LOW) : digitalWrite(KUKA_MOTOR0_ACT, LOW);

				#ifdef DEBUG
				Serial.print("stop");
				#endif
			}
			// Otherwise deal with the smooth acceleration/deceleration (if enabled)
			else if (dual_servo.soft_start && !dual_servo.soft_start_tmp_disabled)
			{
				if (!dual_servo.finished_accelerating)
				{
					dual_servo.steps_to_next_stage--;
					if (dual_servo.steps_to_next_stage == 0)
					{
						dual_servo.soft_start_stage++;
						if (dual_servo.soft_start_stage >= SOFT_START_STAGES)
						{
							dual_servo.finished_accelerating = true;
						}
						// Arithetic progression of the steps to maintain constant acceleration
						dual_servo.steps_to_next_stage = (int32_t) SOFT_START_FIRST_STAGE_STEPS * dual_servo.soft_start_stage;
						// Increment speed
						err = modify_speed(i, dual_servo.speed_increment);
						if (err)
							log_error(err, "Error in TIMER4_OVF_vect", true);

						#ifdef DEBUG
						Serial.print("Stage: ");
						Serial.println(dual_servo.soft_start_stage);
						#endif
					}
				}
				else if (dual_servo.remaining_steps[i] != ROTATE_FOREVER &&
						 dual_servo.remaining_steps[i] <= (int32_t) SOFT_START_NTH_STAGE_SUM(dual_servo.soft_start_stage - 1))
				{
					dual_servo.soft_start_stage--;
					// Decrement speed
					err = modify_speed(i, -dual_servo.speed_increment);
					if (err)
						log_error(err, "Error in TIMER4_OVF_vect", true);

					#ifdef DEBUG
					Serial.print("Stage:");
					Serial.println(dual_servo.soft_start_stage);
					#endif
				}
			}
		}
	}
}

// Executes when any pin in port K changes state and dispatches the corresponding function
ISR(PCINT2_vect)
{
	uint8_t PCI2_new_states = PINK;
	uint8_t PCI2_changed_states = PCI2_new_states ^ PCI2_states;
	PCI2_states = PCI2_new_states;

	if (PCI2_changed_states & EMSTOP_MASK)
		FAILSAFE_ISR();
	if (PCI2_changed_states & (MOTOR0_RDY_MASK | MOTOR1_RDY_MASK))
		MOTOR_RDY_ISR();
	if (PCI2_changed_states & MOTOR0_PREHOME_POS_MASK)
		MOTOR0_PREHOME_POS_ISR();
}
