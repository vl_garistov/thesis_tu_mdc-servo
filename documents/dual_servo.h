/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: dual_servo.h
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Functions for controlling the servo drivers
*/

#ifndef DUAL_SERVO_H
#define DUAL_SERVO_H

#include "Arduino.h"
#include <inttypes.h>

// ########################### PLC pins ##############################

#define KUKA_MACRO0			I0_0
#define KUKA_MACRO1			I1_1
#define KUKA_MACRO2			I1_2
#define KUKA_MACRO3			I1_3
#define KUKA_MOTOR_SELECT	I1_4
#define KUKA_EXECUTE		I1_5
#define KUKA_MOTOR0_ACT		Q0_0
#define KUKA_MOTOR1_ACT		Q0_1

#define MOTOR0_STEP			Q1_7
#define MOTOR1_STEP			Q1_5
#define MOTOR0_DIR			Q1_4
#define MOTOR1_DIR			Q1_3
#define BREAK_RELEASE		Q1_2
#define MOTOR0_EN			Q1_1
#define MOTOR1_EN			Q1_0
#define MOTOR0_RDY			I1_11
#define MOTOR1_RDY			I1_10
#define MOTOR0_PREHOME_POS	I1_9
#define MOTOR1_PREHOME_POS	I1_6
#define UNUSED_4			I0_5
#define UNUSED_5			I0_6
#define UNUSED_0			I2_7
#define UNUSED_2			I2_9
#define UNUSED_1			I2_8
#define UNUSED_3			I2_10

#define MOTOR_SELECT		I2_0
#define STOP_AT_HOME		I2_1
#define MANUAL_FORWARD		I2_6
#define MANUAL_BACKWARD		I2_5
#define MANUAL_MODE			I2_2
#define KUKA_MODE			I2_3

#define REF_POINT_LED		Q2_3

#define EMSTOP	I1_12

// ########################### Pin masks ##############################

#define EMSTOP_MASK				(1 << PINK3)
#define MOTOR0_RDY_MASK			(1 << PINK2)
#define MOTOR1_RDY_MASK			(1 << PINK1)
#define MOTOR0_PREHOME_POS_MASK	(1 << PINK0)
#define MOTOR0_ENC_A_MASK		(1 << PINK4)
#define MOTOR0_ENC_B_MASK		(1 << PINK5)
#define MOTOR1_ENC_A_MASK		(1 << PINK6)
#define MOTOR1_ENC_B_MASK		(1 << PINK7)

// ########################### Constants ##############################

#define NUMBER_OF_MOTORS	2

#define GEAR_RATIO			10UL
//#define STEP_RESOLUTION	4096UL
#define STEP_RESOLUTION		1024UL
// The emulated encoder has a resolution of 4096. However, the decoding logic in the FPGA counts every step as 4 substeps.
#define ENCODER_RESOLUTION	16384UL
// NOTE: if ENCODER_RESOLUTION is changed, function RS232_receive() needs to be adjusted!
#define PULSES_PER_ROTATION	(GEAR_RATIO * ENCODER_RESOLUTION)
// For some ungodly reason the servo drivers ignore every other STEP pulse.
// *Fixed* (circumvented) by chaning the step resolution in the servo driver parameters
#define STEPS_PER_ROTATION	(GEAR_RATIO * STEP_RESOLUTION)
/*
	Estimated maximum speed of the motor based on the nominal speed and the datasheet for other motors from
	this manufacturer. Yeah, no datasheet available for this specific motor.
	Also, in reality it's more like 183.105 rpm because the timer period is rounded up.
	This assumes that ISR(TIMER4_OVF_vect) can be executed in 128 clock cycles. It almost definetely can't.
	TODO: Determine the actual maximum speed. Maybe around 50 rpm?
	max speed is 17.881 because of the FPGA digital filters, add some slack and call it 16.0
*/
//#define MAX_SPEED			200.0
#define MAX_SPEED			16.0
#define MIN_SPEED			(125000.0 * 60.0 / ((float) STEPS_PER_ROTATION * 65535.0))	// approximately 0.0112 rpm
							/*	|		 |										|
								|	seconds in a minute					max timer counter value
								timer clock frequency											*/
#define SLOW_SPEED			0.2
#define DEFAULT_SPEED		1.0

#define FORWARD				1
#define BACKWARD			(-1)
#define CLOCKWISE			BACKWARD
#define ANTI_CLOCKWISE		FORWARD

#define ROTATE_FOREVER		(-1)
#define USE_GUI_SPEED		(0.0)

#define MACRO_QUEUE_LENGHT	20

#define SOFT_START_STAGES				14UL
#define SOFT_START_FIRST_STAGE_STEPS	2UL
// Arithmetic progression
#define SOFT_START_NTH_STAGE_STEPS(n)	((n) * SOFT_START_FIRST_STAGE_STEPS)
#define SOFT_START_NTH_STAGE_SUM(n)		(((n) * (SOFT_START_NTH_STAGE_STEPS(1) + SOFT_START_NTH_STAGE_STEPS(n))) / 2)
#define SOFT_START_TOTAL_STEPS			(2 * SOFT_START_NTH_STAGE_SUM(SOFT_START_STAGES) + 1)	// 1681 steps

// Codes from the FPGA module
#define MOTOR_0_AT_HOME		0x40
#define MOTOR_1_AT_HOME		0x80
#define MOTOR_0_NEW_POS		0x10
#define MOTOR_1_NEW_POS		0x20
#define INIT_COMPLETE_MSG	0x08

// Because boss says the range should be -180 to 180 instead of 0 to 360
#define GLOBAL_OFFSET		180L

#define GUI_ABSOLUTE

// small bifield for saving previous values of the button pins and detecting changes
typedef struct
{
	uint8_t forward : 1;
	uint8_t backward : 1;
}
button_states_t;

// operating mode of the system
typedef enum __attribute__((__packed__))
{
	disabled = 0,
	manual = 1,
	kuka = 2,
	end_of_calibration = 3
}
dual_servo_mode_t;

// TODO: maybe replace the comparison to zero with this?
typedef struct
{
	float speed;
	bool use_gui_speed;
}
servo_speed_t;

typedef struct dual_servo_struct_t
{
	int32_t remaining_steps[NUMBER_OF_MOTORS];
	// 32-bit because STEPS_PER_ROTATION used to be bigger and still might change
	uint32_t pos[NUMBER_OF_MOTORS];
	uint32_t real_pos[NUMBER_OF_MOTORS];
	uint32_t real_pos_offset[NUMBER_OF_MOTORS];
	float speed;
	int8_t dir[NUMBER_OF_MOTORS];
	bool ready[NUMBER_OF_MOTORS];
	bool calibrated[NUMBER_OF_MOTORS];

	dual_servo_mode_t mode;
	uint8_t selected_motor;
	bool stop_at_home;
	bool close_to_home;

	float speed_increment;
	bool soft_start;
	bool soft_start_tmp_disabled;
	uint8_t soft_start_stage;
	uint16_t steps_to_next_stage;
	bool finished_accelerating;
}
dual_servo_t;

typedef enum
{
	IDLE,
	UPD_MOTOR_0,
	UPD_MOTOR_1
}
RS232_receiver_state_t;

typedef union
{
	uint32_t new_pos;
	uint8_t bytes[4];
}
RS232_buffer_t;

// Initializes the two servo drivers and the associated I/O. Must be called in setup()
int dual_servo_init(void);
// Rotate one of the motors (0 or 1) with a given number of steps
// speed is in rpm, direction is either CLOCKWISE or ANTI_CLOCKWISE (FORWARD or BACKWARD)
int start_motor(uint8_t motor, int8_t direction, int32_t steps, float speed);
// Stop a motor that is currently moving
int stop_motor(uint8_t motor);
// Enable smooth acceleration and deceleration. It is enabled by default
int soft_start_enable(void);
// Disable smooth acceleration and deceleration
int soft_start_disable(void);
// Check if smooth acceleration and deceleration is enabled
bool soft_start_get(void);
// Check if a given motor is currently moving
bool motor_active(uint8_t motor);
// Check if a given motor is calibrated
bool motor_calibrated(uint8_t motor);
// Handle a byte of data from the FPGA feedback decoding module
void RS232_receive(uint8_t recv_byte);
// Waits for the release of STOP_AT_HOME, if calibration has just finished
// Must be called in loop()
void dual_servo_loop(void);

#endif
