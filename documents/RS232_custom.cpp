#include "RS232_custom.h"
#include "../../dual_servo.h"

#if defined(HAVE_RS232)

#if defined(HAVE_RS232_SOFT)
RS232CustomClass::RS232CustomClass(uint8_t rxPin, uint8_t txPin) : SoftwareSerial(rxPin, txPin) {
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void RS232CustomClass::begin(unsigned long baud) {
	pinMode(RS232_TX, OUTPUT);
	pinMode(RS232_RX, INPUT);

	SoftwareSerial::begin(baud);
}
RS232CustomClass RS232_custom(RS232_RX, RS232_TX);
#endif // HAVE_RS232_SOFT

#if defined(HAVE_RS232_HARD)

#include <HardwareSerial_private.h>

#if RS232_HWSERIAL == 1
ISR(USART1_RX_vect)
{
	RS232_custom._rx_complete_irq();
}
ISR(USART1_UDRE_vect)
{
	RS232_custom._tx_udr_empty_irq();
}
HardwareSerial RS232_custom(&UBRR1H, &UBRR1L, &UCSR1A, &UCSR1B, &UCSR1C, &UDR1);
#elif RS232_HWSERIAL == 2
ISR(USART2_RX_vect)
{
	if (bit_is_clear(UCSR2A, UPE0))
	{
		RS232_receive(UDR2);
	}
	else
	{
		// Parity error, read byte but discard it
		(void) UDR2;
	};
}
ISR(USART2_UDRE_vect)
{
	RS232_custom._tx_udr_empty_irq();
}
HardwareSerial RS232_custom(&UBRR2H, &UBRR2L, &UCSR2A, &UCSR2B, &UCSR2C, &UDR2);
#elif RS232_HWSERIAL == 3
ISR(USART3_RX_vect)
{
	RS232_custom._rx_complete_irq();
}
ISR(USART3_UDRE_vect)
{
	RS232_custom._tx_udr_empty_irq();
}
HardwareSerial RS232_custom(&UBRR3H, &UBRR3L, &UCSR3A, &UCSR3B, &UCSR3C, &UDR3);
#endif // RS232_HWSERIAL

#endif // HAVE_RS232_HARD

void RS232CustomEvent() __attribute__((weak));

void RS232CustomEventRun() {
	if (RS232CustomEvent && RS232_custom.available()) {
		RS232CustomEvent();
	}
}

#endif // HAVE_RS232
