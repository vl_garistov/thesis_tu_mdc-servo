/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: stacklight.cpp
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Functions for controlling the stacklight. UNUSED!

	Note: The buzzer module of the stacklight is controlled by buzzer.cpp.
*/

#include "stacklight.h"
#include "Arduino.h"

// Initializes the stacklight, must be called in setup()
void stacklight_init(void)
{
	pinMode(SL_RED, OUTPUT);
	pinMode(SL_YELLOW, OUTPUT);
	pinMode(SL_GREEN, OUTPUT);
	digitalWrite(SL_RED, HIGH);
	digitalWrite(SL_YELLOW, HIGH);
	digitalWrite(SL_GREEN, HIGH);
}

/*
	Turn on selected colors of the stacklight.
	mask is a bitwise OR combination of SL_GLOW_RED, SL_GLOW_YELLOW and SL_GLOW_GREEN.
	mask can be SL_GLOW_NONE to turn off all colors.
*/
int set_sl_glow(sl_light_mask_t mask)
{
	// Check for invalid values of the mask
	if ((mask | SL_GLOW_RED | SL_GLOW_YELLOW | SL_GLOW_GREEN) != (SL_GLOW_RED | SL_GLOW_YELLOW | SL_GLOW_GREEN))
	{
		return 501;
	}

	// If SL_GLOW_NONE is selected, turn off all lights
	if (mask == SL_GLOW_NONE)
	{
		digitalWrite(SL_RED, HIGH);
		digitalWrite(SL_YELLOW, HIGH);
		digitalWrite(SL_GREEN, HIGH);
		return 0;
	}

	// Turn on selected colors
	if (mask & SL_GLOW_RED)
		digitalWrite(SL_RED, LOW);
	else
		digitalWrite(SL_RED, HIGH);

	if (mask & SL_GLOW_YELLOW)
		digitalWrite(SL_YELLOW, LOW);
	else
		digitalWrite(SL_YELLOW, HIGH);

	if (mask & SL_GLOW_GREEN)
		digitalWrite(SL_GREEN, LOW);
	else
		digitalWrite(SL_GREEN, HIGH);

	return 0;
}
